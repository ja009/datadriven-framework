package com.webautomation.tests;

import com.webautomation.core.SQLConnection;
import com.webautomation.core.TestBase;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;

public class ItemSearchValidation extends TestBase {


    @Test(enabled = false)
    public void validateUserCanLogIn() throws IOException {
        homePage.validateHomePageLoaded();
        //homePage.validateUserCanSignIn();
        signInPage.insertingCredentials();
        waitFor(7);
    }

    @Test
    public void itemSearch() {
        SQLConnection.establishSqlDbConnection();
        homePage.validateHomePageLoaded();
        homePage.insertItemName();
        waitFor(5);
    }





}
