package com.webautomation.pages;

import com.webautomation.SqlQuery;
import com.webautomation.core.ExtentTestManager;
import com.webautomation.core.SQLConnection;
import com.webautomation.core.TestBase;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

//PAGE OBJECT MODEL FRAMEWORK

public class HomePage extends SQLConnection {

    private static final Logger LOGGER = Logger.getLogger(HomePage.class);

    @FindBy(id = "nav-link-accountList-nav-line-1")
    private WebElement homePageOfAmazon;

    @FindBy(id = "twotabsearchtextbox")
    private WebElement searchBox;

    @FindBy(id = "nav-search-submit-button")
    private WebElement searchButton;

    @FindBy(id = "nav-logo-sprites")
    private WebElement amazonHomeButton;

    @FindBy(xpath = "//div[@class=\"a-section a-spacing-small a-spacing-top-small\"]")
    private WebElement resultNumbers;



    public void validateHomePageLoaded() {
        String currentUrl = TestBase.driver.getCurrentUrl();
        ExtentTestManager.log("Current Url displays homepage");

        String message = homePageOfAmazon.getText();
        Assert.assertTrue(homePageOfAmazon.isDisplayed());
        Assert.assertEquals("Hello, Sign in", message, "Sign-In wasn't correctly displayed");
        ExtentTestManager.log(message + " is displayed and matches as expected");
    }

    public void insertItemName()  {
        try {
            establishSqlDbConnection();
            Statement statement = connection.createStatement();


            ResultSet rs = statement.executeQuery("SELECT * FROM classicmodels.AmazonItemList");
            ArrayList<String> listOfItemNames = new ArrayList<>();

            while (rs.next()) {
                String itemNames = rs.getString(1);
                listOfItemNames.add(itemNames);
            }

            for (String listOfItemName : listOfItemNames) {
                searchBox.sendKeys(listOfItemName);
                ExtentTestManager.log("Items are being searched from the database");
                searchButton.click();
                ExtentTestManager.log("Search button is being clicked");
                String result = resultNumbers.getText();
                ExtentTestManager.log("The result for each item is being stored");
                System.out.println(result);

                String finalUpdateQuery = String.format(SqlQuery.updateQueryStructure, result, listOfItemName);
                PreparedStatement preparedStmt = connection.prepareStatement(finalUpdateQuery);
                preparedStmt.execute();

                amazonHomeButton.click();
                ExtentTestManager.log("Home button is clicked to refresh the Amazon Homepage");
                searchBox.click();
                ExtentTestManager.log("Clicking on search box to search for the next item");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    public void clickingSearchButton(){
//        searchButton.click();
//        ExtentTestManager.log("Search button is clicked");
//    }






    /*public void validateUserCanSignIn(){
        String currentUrl = TestBase.driver.getCurrentUrl();
        ExtentTestManager.log("Current Url displays Sign-in page");
        signIn.click();
        ExtentTestManager.log("Sign-in page is loaded");

    }
*/


}
