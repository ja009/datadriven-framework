package com.webautomation.pages;

import com.google.j2objc.annotations.Weak;
import com.webautomation.core.ExtentTestManager;
import com.webautomation.core.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;

public class SignInPage {

    @FindBy(id = "ap_email")
    private WebElement emailInput;

    @FindBy(id = "continue")
    private WebElement continueButton;

    @FindBy(id = "ap_password")
    private WebElement passInput;

    @FindBy(id = "signInSubmit")
    private WebElement signInButton;

    public void insertingCredentials() throws IOException {

        emailInput.sendKeys(TestBase.getPropertyOfFile("src/main/resources/cloudcred.properties","email"));
        continueButton.click();
        TestBase.waitFor(3);
        ExtentTestManager.log("User was able to insert email and click continue button");
        passInput.sendKeys(TestBase.getPropertyOfFile("src/main/resources/cloudcred.properties","password"));
        signInButton.click();
        TestBase.waitFor(3);
        ExtentTestManager.log("User was able to insert password and click sign-in button");

    }

}
