package com.webautomation.core;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SQLConnection extends TestBase {
    public static Connection connection;
    public static PreparedStatement ps;

    public static void establishSqlDbConnection() {
        try {
            String username = getPropertyOfFile("src/main/resources/dbcred.properties", "username"); // "root";
            String password = getPropertyOfFile("src/main/resources/dbcred.properties", "pass");

            // drivername :// host name : port number/dbName
            String url = "jdbc:mysql://localhost:3306/classicmodels";

            connection = DriverManager.getConnection(url, username, password);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertDataFromStringToSqlTable(String ArrayData, String tableName, String columnName) {
        try {
            establishSqlDbConnection();
            ps = connection.prepareStatement("INSERT INTO " + tableName + " ( " + columnName + " ) VALUES(?)");
            ps.setString(1, ArrayData);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
