package com.webautomation.core;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import com.webautomation.pages.HomePage;
import com.webautomation.pages.SignInPage;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {
    public static WebDriver driver;
    private static ExtentReports extent;

    // store the creds in the properties file and read from there
    public HomePage homePage;
    public SignInPage signInPage;



    public static void waitTillClickable(WebElement element) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(10));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitFor(int seconds) {
        try {
            // static sleep
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @BeforeMethod
    @Parameters({"browserName", "url", "cloud", "cloudOpt", "os"})
    public void setupBrowser(String browserName, String url, boolean cloud, String cloudOpt, String os) throws IOException {
        if (cloud) {
            if(cloudOpt.equalsIgnoreCase("sl")) {//find out why difference between == and equalignorecase
                driver = getRemoteWebDriverSL();
            }else if(cloudOpt.equalsIgnoreCase("bs")){
                driver = getRemoteWebDriverBS();
            }
        } else {
            driver = getLocalWebDriver(browserName, os);
        }
        driver.get(url);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        homePage = PageFactory.initElements(driver, HomePage.class);
        signInPage = PageFactory.initElements(driver, SignInPage.class);
    }

    private WebDriver getLocalWebDriver(String browserName, String os) {
        WebDriver driver;
        if (browserName.equalsIgnoreCase("chrome")) {
            if (os.equalsIgnoreCase("mac")) {
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
            } else {
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
            }
            driver = new ChromeDriver();
        } else {
            if (os.equalsIgnoreCase("mac")) {
                System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
            } else {
                System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
            }
            driver = new FirefoxDriver();
        }
        return driver;
    }

    private static WebDriver getRemoteWebDriverSL() throws IOException {
        FileInputStream files = new FileInputStream("src/main/resources/cloudcred.properties");
        Properties prop = new Properties();
        prop.load(files);

        String slKeys = prop.getProperty("SAUCE_USERNAME");
        String slAccessKeys = prop.getProperty("SAUCE_ACCESS_KEY");
        final String URL_OF_SAUCE = "https://" + slKeys + ":" + slAccessKeys + "@ondemand.us-west-1.saucelabs.com:443/wd/hub";


        ChromeOptions browserOptions = new ChromeOptions();
        browserOptions.setPlatformName("macOS 11.00");
        browserOptions.setBrowserVersion("98");
        Map<String, Object> sauceOptions = new HashMap<>();
        browserOptions.setCapability("sauce:options", sauceOptions);

        WebDriver driver = new RemoteWebDriver(new URL(URL_OF_SAUCE), browserOptions);
        return driver;
    }


    //IN PROGRESS
    private static WebDriver getRemoteWebDriverBS() throws IOException {

        FileInputStream files = new FileInputStream("src/main/resources/cloudcred.properties");
        Properties prop = new Properties();
        prop.load(files);

        String bsKeys = prop.getProperty("BROWSERSTACK_USERNAME");
        String bsAccessKeys = prop.getProperty("BROWSERSTACK_ACCESS_KEY");
        final String URL_OF_BS = "https://" + bsKeys + ":" + bsAccessKeys + "@hub-cloud.browserstack.com/wd/hub";

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("browserName", "Chrome");
        capabilities.setCapability("browserVersion", "latest");

        HashMap<String, Object> browserstackOptions = new HashMap<>();
        browserstackOptions.put("os", "OS X");
        browserstackOptions.put("osVersion", "Big Sur");

        capabilities.setCapability("bstack:options", browserstackOptions);

        WebDriver driver = new RemoteWebDriver(new URL(URL_OF_BS), capabilities);
        return driver;
    }


    @AfterMethod
    public void quitBrowser() {
        driver.quit();
    }

    public void scrollDownToSpecificElement(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", element);
        waitFor(5);
    }

    //Extent Report Setup
    @BeforeSuite(alwaysRun = true)
    public void extentSetup(ITestContext context) {
        ExtentTestManager.setOutputDirectory(context);
        extent = ExtentTestManager.getInstance();
    }

    //Extent Report Setup for each methods
    @BeforeMethod(alwaysRun = true)
    public void startExtent(Method method) {
        String className = method.getDeclaringClass().getSimpleName();
        ExtentTestManager.startTest(method.getName());
        ExtentTestManager.getTest().assignCategory(className);
    }

    //Extent Report cleanup for each methods
    @AfterMethod(alwaysRun = true)
    public void afterEachTestMethod(ITestResult result) {
        ExtentTestManager.getTest().getTest().setStartedTime(ExtentTestManager.getTime(result.getStartMillis()));
        ExtentTestManager.getTest().getTest().setEndedTime(ExtentTestManager.getTime(result.getEndMillis()));
        for (String group : result.getMethod().getGroups()) {
            ExtentTestManager.getTest().assignCategory(group);
        }

        if (result.getStatus() == 1) {
            ExtentTestManager.getTest().log(LogStatus.PASS, "TEST CASE PASSED : " + result.getName());
        } else if (result.getStatus() == 2) {
            ExtentTestManager.getTest().log(LogStatus.FAIL, "TEST CASE FAILED : " + result.getName() + " :: " + ExtentTestManager.getStackTrace(result.getThrowable()));
        } else if (result.getStatus() == 3) {
            ExtentTestManager.getTest().log(LogStatus.SKIP, "TEST CASE SKIPPED : " + result.getName());
        }
        ExtentTestManager.endTest();
        extent.flush();
        if (result.getStatus() == ITestResult.FAILURE) {
            ExtentTestManager.captureScreenshot(driver, result.getName());
        }
    }

    //Extent Report closed
    @AfterSuite(alwaysRun = true)
    public void generateReport() {
        extent.close();
    }

    public static String getPropertyOfFile(String filePath, String key) throws IOException {
        Properties properties = new Properties();
        FileInputStream fs = new FileInputStream(filePath);
        properties.load(fs);
        String value = properties.getProperty(key);
        return value;

    }



}
